import EntryRoutes from './routes/entryRoutes';
import './App.css';
import { ClassNames } from '@emotion/react';



function App() {
  return (
    <>
      <EntryRoutes />
    </>
  );
}

export default App;
