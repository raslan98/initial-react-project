import { Grid } from '@mui/material';
import MoreVertIcon from '@mui/icons-material/MoreVert';
import PropertyData from '../../data/property.json';
import Box from '@mui/material/Box';
import BoxLayoutTable from '../../components/box-layout-table';
import Navbar from '../../components/navbar';
import Funnel from '../../assets/funnel.svg';
import './style.css';
import { useState } from 'react';

const  width = [1.5,2,2,2,1.5,1.5,0.5,'auto'];

function PropertyList() {

    const[list, setList]= useState(PropertyData);

    return(
        <Box className='paper-layout'>
            <Navbar title="Properties" />
            <BoxLayoutTable>
                <Grid container className='search-bar'>
                    <Grid item xs={6}>
                        <input type='search' className='search' placeholder='Search Properties' />
                    </Grid>
                    <Grid item xs={6} textAlign='right'>
                        <img src={Funnel} alt="funnel" className='funnel' />
                    </Grid>
                </Grid>
                <Grid container className='table-header'>
                    <Grid item xs={width[0]}>
                    Property Num
                    </Grid>
                    <Grid item xs={width[1]}>
                    Property Name
                    </Grid>
                    <Grid item xs={width[2]}>
                    Company Name
                    </Grid>
                    <Grid item xs={width[3]}>
                    Location
                    </Grid>
                    <Grid item xs={width[4]}>
                    Revenue Type
                    </Grid>
                    <Grid item xs={width[5]}>
                    Property Type
                    </Grid>
                    <Grid item xs={width[6]}>
                    Status
                    </Grid>
                    <Grid item xs={width[7]}>
                    </Grid>
                </Grid>
                {PropertyData.map((items) =>(
                    <Grid container className='table-property-list'>
                    <Grid item xs={width[0]}>
                    {items.propertynum}
                    </Grid>
                    <Grid item xs={width[1]}>
                    {items.propertyname}
                    </Grid>
                    <Grid item xs={width[2]}>
                    {items.company}
                    </Grid>
                    <Grid item xs={width[3]}>
                    {items.location}
                    </Grid>
                    <Grid item xs={width[4]}>
                    {items.revenue}
                    </Grid>
                    <Grid item xs={width[5]}>
                    {items.propertytype}
                    </Grid>
                    <Grid item xs={width[6]}>
                    {items.status}
                    </Grid>
                    <Grid item xs={width[7]}>
                    <MoreVertIcon />
                    </Grid>
                </Grid>
                ))}
            </BoxLayoutTable>
        </Box>
    )
}

export default PropertyList ;