import Box from '@mui/material/Box';
import './style.css';

function Footer() {
    return(
        <Box textAlign='right' className='footer' mx={2} py={2}>
            <button className='footer-button'>Cancel</button>
            <button className='footer-button'>Create</button>
        </Box>
    )
}

export default Footer;