import Box from '@mui/material/Box';
import { useNavigate } from "react-router-dom";
import './style.css';
import KeyboardArrowRightIcon from '@mui/icons-material/KeyboardArrowRight';
import MenuIcon from '../../../assets/menu-icon.svg';
import LeadsIcon from '../../../assets/leads.png';


function Menu() {
    
    let navigate = useNavigate();
    const routeChange = () => {
        let path = 'property-list';
        navigate(path);
    }

    const routeChangeBack = () => {
        let pathBack = 'property';
        navigate(pathBack)
    }

    return(
        <Box className='menu' id='menu'>
            <Box mb={2}>
                {/* <span>Property Manager for startup</span> */}
                <KeyboardArrowRightIcon className='menu-arrow-icon'/>
            </Box>
            <Box mb={2}>
                <img src={MenuIcon} alt="menu-icon" onClick={routeChange} className="menu-icon" />
            </Box>
            <Box>
                <img src={LeadsIcon} alt="leads-icon" onClick={routeChangeBack}/>
            </Box>
        </Box>
    )
}
export default Menu;

