import Box from '@mui/material/Box';
import './style.css';
import MainLogo from '../../../assets/property-logo.png';
import Avatar from '../../../assets/avatar.png';
import NotificationsOutlinedIcon from '@mui/icons-material/NotificationsOutlined';
import ArrowDownwardOutlinedIcon from '@mui/icons-material/ArrowDownwardOutlined';

function Header() {
    return(
        <Box className='header'>
            <Box className='header-content'>
                <img src={MainLogo} alt='Main-logo' className='header-logo' />
                <span className='line'>|</span>
                <label className='header-name'>PROPERTY MANAGEMENT SOLUTION</label>
            </Box>
            <Box textAlign='right' className='avatar'>
                <Box>
                <NotificationsOutlinedIcon className='bell-icon'/>
                <img src={Avatar} alt="avatar" className='header-avatar' /> 
                <label className='avatar-name'>Bala Vignesh</label>
                <label className='avatar-role'>Frontend developer</label>
                <ArrowDownwardOutlinedIcon fontSize='small'  className='header-arrow'/>
               </Box>
            </Box>
        </Box>
    )
}
export default Header;