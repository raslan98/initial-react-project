import Box from '@mui/material/Box';
import { useForm } from "react-hook-form";
import BoxLayout from '../../components/box-layout';
import InputField from '../../components/input-field';
import FormRow from '../../components/form-row'
import Navbar from '../../components/navbar';
import PlainBuilding from '../../assets/plain-building.png';
import './style.css';


function CreatePropertyPage() {

    const { control, handleSubmit, formState: { errors } } = useForm();
    const save = (data) => {
        console.log(data);
    }

    return (
        <Box className='paper-layout'>
            <Navbar title="Create New Property" />

            <Box display='flex' my={3} ml={3} mr={2}>
                <Box width={206} textAlign="center">
                    <BoxLayout>
                        <label className='box-layout-content'>PROPERTY IMAGE</label>
                        <img src={PlainBuilding} alt="plain-building" className='box-layout-img' />
                        <button className='box-layout-button'>Upload Image</button>
                    </BoxLayout>
                </Box>
                <Box flex={1} ml={2}>
                    <BoxLayout>
                        <label className='box-layout-content'>PROPERTY DETAILS</label>
                        <Box display='flex'>
                            <Box flex={1} my={1} mr={1}>
                                <FormRow label="Property Type">
                                    <InputField
                                        name="propertyType"
                                        type="email"
                                        placeholder="Enter Your Property Type"
                                        control={control}
                                        errors={errors}
                                    />
                                </FormRow>
                            </Box>
                            <Box flex={1} px={1} my={1}>
                                <FormRow label="Property Name">
                                    <InputField
                                        name="propertyName"
                                        type="text"
                                        placeholder="Enter Your Property Name"
                                        control={control}
                                        errors={errors}
                                    />
                                </FormRow>
                            </Box>
                            <Box flex={1} px={1} my={1}>
                                <FormRow label="Payment Period">
                                    <InputField
                                        name="paymentPeriod"
                                        type="email"
                                        placeholder="Enter Your Paymnet Period"
                                        control={control}
                                        errors={errors}
                                    />
                                </FormRow>
                            </Box>
                            <Box flex={1} px={1} my={1}>
                                <FormRow label="Status">
                                    <InputField
                                        name="status"
                                        type="email"
                                        placeholder="Enter Your Status"
                                        control={control}
                                        errors={errors}
                                    />
                                </FormRow>
                            </Box>
                        </Box>
                        <Box>
                            <Box flex={1} px={1} my={3}>
                                <FormRow label="Property Description">
                                    <InputField
                                        name="desc"
                                        type="email"
                                        placeholder="Enter Your Property Description"
                                        control={control}
                                        errors={errors}
                                    />
                                </FormRow>
                            </Box>
                        </Box>
                    </BoxLayout>
                </Box>
            </Box>

            <Box my={1} ml={3} mr={2}>
                <form onSubmit={handleSubmit(save)}>
                    <BoxLayout>
                        <Box display='flex'>
                            <Box flex={1} px={1}>
                                <FormRow label="Property Type">
                                    <InputField
                                        name="PropertyType"
                                        type="text"
                                        placeholder="Enter Your Property Type"
                                        control={control}
                                        errors={errors}
                                    />
                                </FormRow>
                            </Box>
                            <Box flex={1} px={1}>
                                <FormRow label="Property Purpose">
                                    <InputField
                                        name="propertyPurpose"
                                        type="email"
                                        placeholder="Enter Your Property Purpose"
                                        control={control}
                                        errors={errors}
                                    />
                                </FormRow>
                            </Box>
                            <Box flex={1} px={1}>
                                <FormRow label="Revenue type">
                                    <InputField
                                        name="revenueType"
                                        type="email"
                                        placeholder="Enter Revenue type"
                                        control={control}
                                        errors={errors}
                                    />
                                </FormRow>
                            </Box>
                            <Box flex={1} px={1}>
                                <FormRow label="Measurement Unit">
                                    <InputField
                                        name="measurementUnit"
                                        type="email"
                                        placeholder="Enter Your Measurement Unit"
                                        control={control}
                                        errors={errors}
                                    />
                                </FormRow>
                            </Box>
                            <Box flex={1} px={1}>
                                <FormRow label="Carpet Area">
                                    <InputField
                                        name="carpetArea"
                                        type="text"
                                        placeholder="Enter Your Carpet Area"
                                        control={control}
                                        errors={errors}
                                    />
                                </FormRow>
                            </Box>
                            <Box flex={1} px={1}>
                                <FormRow label="Total Area">
                                    <InputField
                                        name="totalArea"
                                        type="text"
                                        placeholder="Enter Your Total Area"
                                        control={control}
                                        errors={errors}
                                    />
                                </FormRow>
                            </Box>
                        </Box>
                    </BoxLayout>

                    <BoxLayout>
                        <Box display='flex'>
                            <Box flex={1} px={1}>
                                <FormRow label="Year Built">
                                    <InputField
                                        name="yearBuilt"
                                        type="text"
                                        placeholder="Enter Your Total Area"
                                        control={control}
                                        errors={errors}
                                    />
                                </FormRow>
                            </Box>
                            <Box flex={1} px={1}>
                                <FormRow label="Handover Date">
                                    <InputField
                                        name="handoverDate"
                                        type="text"
                                        placeholder="Enter Your Total Area"
                                        control={control}
                                        errors={errors}
                                    />
                                </FormRow>
                            </Box>
                            <Box flex={1} px={1}>
                                <FormRow label="Public Listing">
                                    <InputField
                                        name="totalArea"
                                        type="text"
                                        placeholder="Enter Your Total Area"
                                        control={control}
                                        errors={errors}
                                    />
                                </FormRow>
                            </Box>
                            <Box flex={1} px={1}>
                                <FormRow label="Pets Allowed">
                                    <InputField
                                        name="totalArea"
                                        type="text"
                                        placeholder="Enter Your Total Area"
                                        control={control}
                                        errors={errors}
                                    />
                                </FormRow>
                            </Box>
                        </Box>
                    </BoxLayout>
                </form>
            </Box>
            <Box my={2} ml={3} mr={2}>
                <BoxLayout>
                    <label className='box-layout-content'>CONTACT & OTHER INFORMATION</label>
                    <Box display='flex'>
                        <Box flex={0.5} px={1} my={1}>
                            <FormRow label="Buiness Phone">
                                <InputField
                                    name="businessPhone"
                                    type="number"
                                    placeholder="Enter Your Phone Number"
                                    control={control}
                                    errors={errors}
                                />
                            </FormRow>
                        </Box>
                        <Box flex={0.5} px={1} my={1}>
                            <FormRow label="Mobile Phone">
                                <InputField
                                    name="mobilePhone"
                                    type="number"
                                    placeholder="Enter Your Phone Number"
                                    control={control}
                                    errors={errors}
                                />
                            </FormRow>
                        </Box>
                        <Box flex={1} px={1} my={1}>
                            <FormRow label="Website">
                                <InputField
                                    name="website"
                                    type="text"
                                    placeholder="Enter Your Website URL"
                                    control={control}
                                    errors={errors}
                                />
                            </FormRow>
                        </Box>
                        <Box flex={1} px={1} my={1}>
                            <FormRow label="Email Adress">
                                <InputField
                                    name="emailAdress"
                                    type="text"
                                    placeholder="Enter Your Email Address"
                                    control={control}
                                    errors={errors}
                                />
                            </FormRow>
                        </Box>
                    </Box>
                </BoxLayout>
            </Box>
        </Box>
    )
}

export default CreatePropertyPage;