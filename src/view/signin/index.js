import React from "react";
import { useForm } from "react-hook-form";
import { yupResolver } from '@hookform/resolvers/yup';
import { useNavigate } from 'react-router-dom';
import * as yup from "yup";
import './style.css';
import { Grid, Box } from '@mui/material';
import Building from '../../assets/building.png';
import Logo from '../../assets/logo.png';
import Subtraction from '../../assets/subtraction-2.svg';
import InputField from "../../components/input-field";
import FormRow from "../../components/form-row";



function SignInPage() {

    const navigate = useNavigate();

    const schema = yup.object({
        phoneNumber: yup.string().required("* This field is required"),
        password: yup.string().required("* This field is required")
    })

    const { control, handleSubmit, formState: { errors } } = useForm({
        resolver: yupResolver(schema),
        // defaultValues: {
        //     phoneNumber: "900000",
        //     password: "crayond",
        // }
    });
    const save = (data) => {
        console.log(data);
        navigate('/property');
    }

    return (
        <>
            <Grid container id="sing-in">
                <Grid item lg={3} md={3} sm={3} >
                    <img src={Subtraction} alt="Blue-rings" className="top-ring" />
                </Grid>
                <Grid item lg={9} md={12} sm={12} className="bg-color-blue view-height">
                    <img src={Building} alt="Building" className="img-building" />
                    <div className="signin-card">
                        <label className="signin-card-description">Sign In</label>
                        <form onSubmit={handleSubmit(save)}>
                            <Box mb={4}>
                                <FormRow label="Mobile Number/ Email Id">
                                    <InputField
                                        name="phoneNumber"
                                        type="email"
                                        placeholder="Enter Your Mobile Number / Email Id"
                                        control={control}
                                        errors={errors}
                                    />
                                </FormRow>
                            </Box>
                            <Box>
                                <FormRow label="Enter Password">
                                    <InputField
                                        name="password"
                                        type="password"
                                        placeholder="Enter Your Password"
                                        control={control}
                                        errors={errors}
                                    />
                                </FormRow>
                            </Box>
                            <div className="forgot-password">Did You forget your password?<a href="_blank">Click Here</a></div>
                            <div className="company-description"><label>Powered by</label><img src={Logo} alt="logo" /><label>Property Automate</label></div>
                            <input type="submit" value="Log In" />
                        </form>
                    </div>
                </Grid>
            </Grid>
        </>
    )
}


export default SignInPage;

