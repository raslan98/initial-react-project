import FieldLabel from "../field-label";

function FormRow(props) {
    return (
        <>
        <FieldLabel name={props.label} />
        {props.children}
        </>
    )
}

export default FormRow;