import './style.css';
import { Controller } from "react-hook-form";

function InputField(props) {
    return (
        <>
            <p className="error-message">{props.errors[props.name]?.message}</p>
            <Controller
                name={props.name}
                control={props.control}
                render={({ field }) => <input {...field} type={props.type} placeholder={props.placeholder} className="input"/>}
            />
        </>
    )
}

export default InputField;