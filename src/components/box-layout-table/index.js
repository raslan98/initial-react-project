import Box from '@mui/material/Box';
import './style.css';

function BoxLayoutTable(props) {
    return(
        <Box px={2} py={2} className="box-layout-table">
            {props.children}
        </Box>
    )
}

export default BoxLayoutTable;