import Box from '@mui/material/Box';
import './style.css';

function BoxLayout(props) {
    return(
        <Box px={2} py={2} className="box-layout">
            {props.children}
        </Box>
    )
}

export default BoxLayout;