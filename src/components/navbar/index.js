import Box from '@mui/material/Box';
import KeyboardArrowLeftRoundedIcon from '@mui/icons-material/KeyboardArrowLeftRounded';
import './style.css';

function Navbar(props) {
    return(
        <Box className='navbar'>
            <KeyboardArrowLeftRoundedIcon className='navbar-icon'/>
            <label className='title'>{props.title}</label>
        </Box>
    )
}

export default Navbar;