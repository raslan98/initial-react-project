import './style.css';

function FieldLabel(props) {
    return(
        <label>{props.name}{props.css}</label>
    )
}

export default FieldLabel;