import { Route, Routes } from 'react-router-dom';
import Box from '@mui/material/Box';
import Menu from '../view/common/menu';
import Header from '../view/common/header';
import CreatePropertyPage from '../view/property';
import PropertyList from '../view/property-list';
import Footer from '../view/common/footer';


function AppRoutes() {
    return (
        <Box>
            <Header />
            <Box display='flex' className='property-page'>
                <Box width={73}>
                    <Menu />
                </Box>
                <Box flex={1}>
                    <Routes>
                        <Route path='/property' element={<CreatePropertyPage />} />
                        <Route path='/property-list' element={<PropertyList />} />
                    </Routes>
                </Box>
            </Box>
            <Footer />
        </Box>
    )
}

export default AppRoutes;