import { BrowserRouter, Route, Routes } from 'react-router-dom';
import AppRoutes from './appRoutes';
import SignInPage from '../view/signin';

function EntryRoutes() {
    return(
        <BrowserRouter>
        <Routes>
          <Route path='/' element={<SignInPage />} />
          <Route path='*' element={<AppRoutes />} />
        </Routes>
      </BrowserRouter>
    )
}

export default EntryRoutes;